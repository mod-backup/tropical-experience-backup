

	table.insert(Assets, Asset("ANIM", "anim/oxygen_meter_player.zip"))
	table.insert(Assets, Asset("ANIM", "anim/hunger_ghost.zip"))
	table.insert(Assets, Asset("ANIM", "anim/squidunderwater.zip"))
	table.insert(Assets, Asset("ANIM", "anim/jellyfish.zip"))
	table.insert(Assets, Asset("ANIM", "anim/sandstone_boulder.zip"))
	table.insert(Assets, Asset("ANIM", "anim/vent.zip"))
	table.insert(Assets, Asset("ANIM", "anim/fish_fillet.zip"))
	table.insert(Assets, Asset("ANIM", "anim/commonfish.zip"))
	table.insert(Assets, Asset("ANIM", "anim/sand.zip"))
	table.insert(Assets, Asset("ANIM", "anim/pearl.zip"))
	table.insert(Assets, Asset("ANIM", "anim/iron_ore.zip"))
	table.insert(Assets, Asset("ANIM", "anim/lavastone.zip"))
	table.insert(Assets, Asset("ANIM", "anim/clam.zip"))
	table.insert(Assets, Asset("ANIM", "anim/sea_eel.zip"))
	table.insert(Assets, Asset("ANIM", "anim/sunken_chest.zip"))
	table.insert(Assets, Asset("ANIM", "anim/kelp.zip"))
	table.insert(Assets, Asset("ANIM", "anim/wormplant.zip"))
	table.insert(Assets, Asset("ANIM", "anim/decorative_shell.zip"))
	table.insert(Assets, Asset("ANIM", "anim/coral_orange.zip"))
	table.insert(Assets, Asset("ANIM", "anim/sponge.zip"))
	table.insert(Assets, Asset("ANIM", "anim/fish_n_chips.zip"))
	table.insert(Assets, Asset("ANIM", "anim/fish_gazpacho.zip"))
	table.insert(Assets, Asset("ANIM", "anim/sponge_cake.zip"))
	table.insert(Assets, Asset("ANIM", "anim/tuna_muffin.zip"))
	table.insert(Assets, Asset("ANIM", "anim/sea_cucumber.zip"))
	table.insert(Assets, Asset("ANIM", "anim/jelly_cap.zip"))
	table.insert(Assets, Asset("ANIM", "anim/shrimp.zip"))
	table.insert(Assets, Asset("ANIM", "anim/diving_suit_summer.zip"))
	table.insert(Assets, Asset("ANIM", "anim/diving_suit_winter.zip"))
	table.insert(Assets, Asset("ANIM", "anim/tentacle_sushi.zip"))
	table.insert(Assets, Asset("ANIM", "anim/flower_sushi.zip"))
	table.insert(Assets, Asset("ANIM", "anim/fish_sushi.zip"))
	table.insert(Assets, Asset("ANIM", "anim/shrimp_tail.zip"))
	table.insert(Assets, Asset("ANIM", "anim/jelly_lantern.zip"))
	table.insert(Assets, Asset("ANIM", "anim/seagrass_chunk.zip"))
	table.insert(Assets, Asset("ANIM", "anim/underwater_entrance.zip"))
	table.insert(Assets, Asset("ANIM", "anim/underwater_exit.zip"))
	table.insert(Assets, Asset("ANIM", "anim/coral_orange_ground.zip"))
	table.insert(Assets, Asset("ANIM", "anim/coral_blue_ground.zip"))
	table.insert(Assets, Asset("ANIM", "anim/coral_green_ground.zip"))
	table.insert(Assets, Asset("ANIM", "anim/snorkel.zip"))
	table.insert(Assets, Asset("ANIM", "anim/seajelly.zip"))
	table.insert(Assets, Asset("ANIM", "anim/coral_cluster.zip"))
	table.insert(Assets, Asset("ANIM", "anim/uw_flowers.zip"))
	table.insert(Assets, Asset("ANIM", "anim/sea_petals.zip"))
	---
	
	-- Inventory images

	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/creepindedeepinventory.xml"))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/creepindedeepinventory.tex" ))	

	-- Minimap Icons
	table.insert(Assets, Asset("IMAGE", "images/minimap/iron_boulder.tex"))
	table.insert(Assets, Asset("ATLAS", "images/minimap/iron_boulder.xml"))
	table.insert(Assets, Asset("IMAGE", "images/minimap/seagrass.tex"))
	table.insert(Assets, Asset("ATLAS", "images/minimap/seagrass.xml"))
	table.insert(Assets, Asset("IMAGE", "images/minimap/kelp.tex"))
	table.insert(Assets, Asset("ATLAS", "images/minimap/kelp.xml"))
	table.insert(Assets, Asset("IMAGE", "images/minimap/vent.tex"))
	table.insert(Assets, Asset("ATLAS", "images/minimap/vent.xml"))
	table.insert(Assets, Asset("IMAGE", "images/minimap/entrance_open.tex"))
	table.insert(Assets, Asset("ATLAS", "images/minimap/entrance_open.xml"))
	table.insert(Assets, Asset("IMAGE", "images/minimap/entrance_closed.tex"))
	table.insert(Assets, Asset("ATLAS", "images/minimap/entrance_closed.xml"))
	table.insert(Assets, Asset("IMAGE", "images/minimap/orange_coral.tex"))
	table.insert(Assets, Asset("ATLAS", "images/minimap/orange_coral.xml"))
	table.insert(Assets, Asset("IMAGE", "images/minimap/clam.tex"))
	table.insert(Assets, Asset("ATLAS", "images/minimap/clam.xml"))
	table.insert(Assets, Asset("IMAGE", "images/minimap/sponge.tex"))
	table.insert(Assets, Asset("ATLAS", "images/minimap/sponge.xml"))
	table.insert(Assets, Asset("IMAGE", "images/minimap/wormplant.tex"))
	table.insert(Assets, Asset("ATLAS", "images/minimap/wormplant.xml"))
	
	-- Sounds
	table.insert(Assets, Asset("SOUNDPACKAGE", "sound/citd.fev"))
	table.insert(Assets, Asset("SOUND", "sound/citd.fsb"))
	
-- Inventory image assets
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/placeholder.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/diving_suit_summer.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/diving_suit_winter.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/snorkel.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/sponge_piece.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/fish_fillet.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/fish_fillet_cooked.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/bubble_item.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/sand.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/pearl.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/iron_ore.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/lavastone.xml" ))
	--table.insert(Assets, Asset("ATLAS", "images/inventoryimages/food/fish_n_chips.xml" ))
	--table.insert(Assets, Asset("ATLAS", "images/inventoryimages/food/sponge_cake.xml" ))
	--table.insert(Assets, Asset("ATLAS", "images/inventoryimages/food/fish_gazpacho.xml" ))
	--table.insert(Assets, Asset("ATLAS", "images/inventoryimages/food/tuna_muffin.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/sea_cucumber.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/jelly_cap.xml" ))
	--table.insert(Assets, Asset("ATLAS", "images/inventoryimages/food/tentacle_sushi.xml" ))
	--table.insert(Assets, Asset("ATLAS", "images/inventoryimages/food/fish_sushi.xml" ))
	--table.insert(Assets, Asset("ATLAS", "images/inventoryimages/food/flower_sushi.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/shrimp_tail.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/jelly_lantern.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/seagrass_chunk.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/cut_orange_coral.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/cut_blue_coral.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/cut_green_coral.xml" ))
	--table.insert(Assets, Asset("ATLAS", "images/inventoryimages/food/seajelly.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/coral_cluster.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/pearl_amulet.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/flare.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/sea_petals.xml" ))
	table.insert(Assets, Asset("ATLAS", "images/inventoryimages/creepindeep_cuisine.xml"))

	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/placeholder.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/diving_suit_summer.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/diving_suit_winter.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/snorkel.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/sponge_piece.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/fish_fillet.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/fish_fillet_cooked.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/bubble_item.tex" ))	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/sand.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/pearl.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/iron_ore.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/lavastone.tex" ))	
	--table.insert(Assets, Asset("IMAGE", "images/inventoryimages/food/fish_n_chips.tex" ))
	--table.insert(Assets, Asset("IMAGE", "images/inventoryimages/food/sponge_cake.tex" ))
	--table.insert(Assets, Asset("IMAGE", "images/inventoryimages/food/fish_gazpacho.tex" ))
	--table.insert(Assets, Asset("IMAGE", "images/inventoryimages/food/tuna_muffin.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/sea_cucumber.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/jelly_cap.tex" ))
	--table.insert(Assets, Asset("IMAGE", "images/inventoryimages/food/tentacle_sushi.tex" ))
	--table.insert(Assets, Asset("IMAGE", "images/inventoryimages/food/fish_sushi.tex" ))
	--table.insert(Assets, Asset("IMAGE", "images/inventoryimages/food/flower_sushi.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/shrimp_tail.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/jelly_lantern.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/seagrass_chunk.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/cut_orange_coral.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/cut_blue_coral.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/cut_green_coral.tex" ))
	--table.insert(Assets, Asset("IMAGE", "images/inventoryimages/food/seajelly.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/coral_cluster.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/pearl_amulet.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/flare.tex" ))
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/sea_petals.tex" ))	
	table.insert(Assets, Asset("IMAGE", "images/inventoryimages/creepindeep_cuisine.tex"))




table.insert(PrefabFiles,"geothermal_vent")
table.insert(PrefabFiles,"pearl_amulet")
table.insert(PrefabFiles,"flare")
table.insert(PrefabFiles,"squidunderwater")
table.insert(PrefabFiles,"jelly_cap")
table.insert(PrefabFiles,"reef_jellyfish")
table.insert(PrefabFiles,"jellyfishschool")
table.insert(PrefabFiles,"underwater_entrance")
table.insert(PrefabFiles,"underwater_exit")
table.insert(PrefabFiles,"diving_suits")
table.insert(PrefabFiles,"seatentacle")
table.insert(PrefabFiles,"seaquaketentacle")	
table.insert(PrefabFiles,"uw_coral")
table.insert(PrefabFiles,"pearl")
table.insert(PrefabFiles,"seagrass")
table.insert(PrefabFiles,"rotting_trunk")
table.insert(PrefabFiles,"hat_snorkel")
table.insert(PrefabFiles,"bubble_vent")
	
table.insert(PrefabFiles,"flower_sea")
table.insert(PrefabFiles,"iron_boulder")
table.insert(PrefabFiles,"iron_ore")
table.insert(PrefabFiles,"sandstone_boulder")
table.insert(PrefabFiles,"sandstone")
table.insert(PrefabFiles,"tidal_node")
table.insert(PrefabFiles,"sponges")
table.insert(PrefabFiles,"sponge_piece")
	
table.insert(PrefabFiles,"fish_fillet")
table.insert(PrefabFiles,"fish_fillet_cooked")

table.insert(PrefabFiles,"bubbles")	
	
table.insert(PrefabFiles,"commonfish")
table.insert(PrefabFiles,"lavastone")
table.insert(PrefabFiles,"clam")
table.insert(PrefabFiles,"sea_eel")
table.insert(PrefabFiles,"kelp")
table.insert(PrefabFiles,"wormplant")
table.insert(PrefabFiles,"decorative_shell")
-- table.insert(PrefabFiles,"citd_preparedfoods")
table.insert(PrefabFiles,"sea_cucumber")
table.insert(PrefabFiles,"commonfishschool")
table.insert(PrefabFiles,"shrimp")
table.insert(PrefabFiles,"shrimp_tail")
table.insert(PrefabFiles,"jelly_lantern")
table.insert(PrefabFiles,"seagrass_chunk")
table.insert(PrefabFiles,"cut_orange_coral")
table.insert(PrefabFiles,"cut_blue_coral")
table.insert(PrefabFiles,"cut_green_coral")
table.insert(PrefabFiles,"uw_flowers")
table.insert(PrefabFiles,"sea_petals")
table.insert(PrefabFiles,"reeflight_small")
table.insert(PrefabFiles,"coral_cluster")
table.insert(PrefabFiles,"coral_fish")	
table.insert(PrefabFiles,"fishesunderwater")
table.insert(PrefabFiles,"dogfish_under")
table.insert(PrefabFiles,"fish_coi")	
table.insert(PrefabFiles,"lobsterunderwater")
table.insert(PrefabFiles,"stungrayunderwater")
table.insert(PrefabFiles,"wreckunderwater")
table.insert(PrefabFiles,"krakenunderwater_tentacle")
table.insert(PrefabFiles,"krakenunderwater")
table.insert(PrefabFiles,"krakenunderwater_spawner")
table.insert(PrefabFiles,"gaze_beamunderwater")
table.insert(PrefabFiles,"rock_cave")
table.insert(PrefabFiles,"coralreefunderwater")
table.insert(PrefabFiles,"coral_brain_rockunderwater")
table.insert(PrefabFiles,"hat_submarine")
table.insert(PrefabFiles,"secretcaveentrance")
table.insert(PrefabFiles,"jellyfish_underwater")
table.insert(PrefabFiles,"rainbowjellyfish_underwater")
table.insert(PrefabFiles,"fishesunderwaternew")
table.insert(PrefabFiles,"kelpy")
table.insert(PrefabFiles,"merm_statue")
table.insert(PrefabFiles,"woodlegs_cage_underwater")	
table.insert(PrefabFiles,"squidundewater2")
table.insert(PrefabFiles,"swordfishunderwater")
table.insert(PrefabFiles,"sharxunderwater")
table.insert(PrefabFiles,"mermkingunderwater")
table.insert(PrefabFiles,"mermthroneunderwater")
table.insert(PrefabFiles,"mermwatchtowerunderwater")
table.insert(PrefabFiles,"mermsunderwater")
table.insert(PrefabFiles,"roe_fish")
table.insert(PrefabFiles,"fish_med")
table.insert(PrefabFiles,"bioluminescence")
table.insert(PrefabFiles,"kraken_projectile_underwater")
table.insert(PrefabFiles,"tropicalspawnblocker")
table.insert(PrefabFiles,"rainbowjellyfish")
table.insert(PrefabFiles,"jellyfish")
table.insert(PrefabFiles,"magma_rocks")
table.insert(PrefabFiles,"limpets")
table.insert(PrefabFiles,"rock_limpet")
table.insert(PrefabFiles,"crate")
table.insert(PrefabFiles,"crabhole")
table.insert(PrefabFiles,"crab")
table.insert(PrefabFiles,"coral")
table.insert(PrefabFiles,"coral_brain")
table.insert(PrefabFiles,"pig_ruins_pressure_plate")
table.insert(PrefabFiles,"pig_ruins_spear_trap")
table.insert(PrefabFiles,"smashingpot")	
table.insert(PrefabFiles,"gnarwailunderwater")
table.insert(PrefabFiles,"seaweedunderwater")	

-- Add minimap atlas
AddMinimapAtlas("images/minimap/iron_boulder.xml")
AddMinimapAtlas("images/minimap/seagrass.xml")
AddMinimapAtlas("images/minimap/kelp.xml")
AddMinimapAtlas("images/minimap/vent.xml")
AddMinimapAtlas("images/minimap/entrance_open.xml")
AddMinimapAtlas("images/minimap/entrance_closed.xml")
AddMinimapAtlas("images/minimap/orange_coral.xml")
AddMinimapAtlas("images/minimap/clam.xml")
AddMinimapAtlas("images/minimap/sponge.xml")
AddMinimapAtlas("images/minimap/wormplant.xml")
	
modimport "uw_tuning"

--RemapSoundEvent("dontstarve/music/music_FE", "dontstarve/music/gramaphone_drstyle") --Again, it's just a placeholder until we have a proper theme

RegisterInventoryItemAtlas("images/inventoryimages/sponge_piece.xml", "sponge_piece.tex" )
RegisterInventoryItemAtlas("images/inventoryimages/fish_fillet.xml", "fish_fillet.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/fish_fillet_cooked.xml", "fish_fillet_cooked.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/fish_n_chips.xml", "fish_n_chips.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/sponge_cake.xml", "sponge_cake.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/fish_gazpacho.xml", "fish_gazpacho.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/tuna_muffin.xml", "tuna_muffin.tex"  )
RegisterInventoryItemAtlas("ATLAS", "images/inventoryimages/sea_cucumber.xml", "sea_cucumber.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/tentacle_sushi.xml", "tentacle_sushi.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/fish_sushi.xml", "fish_sushi.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/flower_sushi.xml", "flower_sushi.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/shrimp_tail.xml", "shrimp_tail.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/sea_petals.xml", "sea_petals.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/seagrass_chunk.xml", "seagrass_chunk.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/sea_petals.xml", "sea_petals.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/jelly_cap.xml", "jelly_cap.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seaweed.tex" )
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seaweed_cooked.tex"  )	
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seaweed_dried"  )

RegisterInventoryItemAtlas("images/inventoryimages/creepindeep_cuisine.xml", "creepindeep_cuisine.tex"  )

-- Mod recipes
modimport "modrecipes"

-- Mod cooking recipes and ingredients
AddIngredientValues({"fish_fillet"}, {fish=1, meat=0.5}, true, false)
AddIngredientValues({"sponge_piece"}, {sponge=1}, false, false)
AddIngredientValues({"seagrass_chunk"}, {sea_veggie=1, veggie=0.5}, false, false)
AddIngredientValues({"trinket_12"}, {tentacle=1, meat=0.5}, false, false)
AddIngredientValues({"petals"}, {flower=1}, false, false)
AddIngredientValues({"sea_petals"}, {flower=1}, false, false)
AddIngredientValues({"jelly_cap"}, {sea_jelly=1}, false, false)
AddIngredientValues({"saltrock"}, {saltrock=1}, false, false)

-- local sponge_cake =
-- {
--     name = "sponge_cake",
-- 		test = function(cooker, names, tags) return tags.dairy and tags.sweetener and tags.sponge and tags.sponge >= 2 and not tags.meat end,
-- 		priority = 0,
-- 		weight = 1,
-- 		foodtype = FOODTYPE.GOODIES,
-- 		health = 0,
-- 		hunger = 25,
-- 		sanity = 50,
-- 		perishtime = TUNING.PERISH_SUPERFAST,
-- 		cooktime = .5,
-- 		cookbook_atlas = "images/inventoryimages/creepindeep_cuisine.xml",		
-- 	floater = {"small", 0.05, 0.7},
--     tags = {},
-- }

-- AddCookerRecipe("cookpot",sponge_cake)
-- AddCookerRecipe("portablecookpot",sponge_cake)
-- AddCookerRecipe("xiuyuan_cookpot",sponge_cake)


-- local fish_n_chips =
-- {
--     name = "fish_n_chips",
-- 		test = function(cooker, names, tags) return tags.fish and tags.fish >= 2 and tags.veggie and tags.veggie >= 2 end,
-- 		priority = 10,
-- 		weight = 1,
-- 		foodtype = "MEAT",
-- 		health = 25,
-- 		hunger = 42.5,
-- 		sanity = 10,
-- 		perishtime = TUNING.PERISH_FAST,
-- 		cooktime = 1,
-- 		cookbook_atlas = "images/inventoryimages/creepindeep_cuisine.xml",			
-- 	floater = {"small", 0.05, 0.7},
--     tags = {},
-- }

-- AddCookerRecipe("cookpot",fish_n_chips)
-- AddCookerRecipe("portablecookpot",fish_n_chips)
-- AddCookerRecipe("xiuyuan_cookpot",fish_n_chips)

-- local tuna_muffin =
-- {
--     name = "tuna_muffin",
-- 		test = function(cooker, names, tags) return tags.fish and tags.fish >= 1 and tags.sponge and tags.sponge >= 1 and not tags.twigs end,
-- 		priority = 5,
-- 		weight = 1,
-- 		foodtype = "MEAT",
-- 		health = 0,
-- 		hunger = 32.5,
-- 		sanity = 10,
-- 		perishtime = TUNING.PERISH_MED,
-- 		cooktime = 2,
-- 		cookbook_atlas = "images/inventoryimages/creepindeep_cuisine.xml",			
-- 	floater = {"small", 0.05, 0.7},
--     tags = {},
-- }

-- AddCookerRecipe("cookpot",tuna_muffin)
-- AddCookerRecipe("portablecookpot",tuna_muffin)
-- AddCookerRecipe("xiuyuan_cookpot",tuna_muffin)
	
-- local tentacle_sushi =
-- {
--     name = "tentacle_sushi",
-- 		test = function(cooker, names, tags) return tags.tentacle and tags.tentacle and tags.sea_veggie and tags.fish >= 0.5 and not tags.twigs end,
-- 		priority = 0,
-- 		weight = 1,
-- 		foodtype = "MEAT",
-- 		health = 35,
-- 		hunger = 5,
-- 		sanity = 5,
-- 		perishtime = TUNING.PERISH_MED,
-- 		cooktime = 2,
-- 		cookbook_atlas = "images/inventoryimages/creepindeep_cuisine.xml",		
-- 	floater = {"small", 0.05, 0.7},
--     tags = {},
-- }

-- AddCookerRecipe("cookpot",tentacle_sushi)
-- AddCookerRecipe("portablecookpot",tentacle_sushi)
-- AddCookerRecipe("xiuyuan_cookpot",tentacle_sushi)

-- local flower_sushi =
-- {
--     name = "flower_sushi",
-- 		test = function(cooker, names, tags) return tags.flower and tags.sea_veggie and tags.fish and tags.fish >= 1 and not tags.twigs end,
-- 		priority = 0,
-- 		weight = 1,
-- 		foodtype = "MEAT",
-- 		health = 10,
-- 		hunger = 5,
-- 		sanity = 30,
-- 		perishtime = TUNING.PERISH_MED,
-- 		cooktime = 2,
-- 		cookbook_atlas = "images/inventoryimages/creepindeep_cuisine.xml",			
-- 	floater = {"small", 0.05, 0.7},
--     tags = {},
-- }

-- AddCookerRecipe("cookpot",flower_sushi)
-- AddCookerRecipe("portablecookpot",flower_sushi)
-- AddCookerRecipe("xiuyuan_cookpot",flower_sushi)

-- local fish_sushi =
-- {
--     name = "fish_sushi",
-- 		test = function(cooker, names, tags) return tags.tentacle and tags.veggie >= 1 and tags.fish and tags.fish >= 1 and not tags.twigs end,
-- 		priority = 0,
-- 		weight = 1,
-- 		foodtype = "MEAT",
-- 		health = 5,
-- 		hunger = 50,
-- 		sanity = 0,
-- 		perishtime = TUNING.PERISH_MED,
-- 		cooktime = 2,
-- 		cookbook_atlas = "images/inventoryimages/creepindeep_cuisine.xml",			
-- 	floater = {"small", 0.05, 0.7},
--     tags = {},
-- }

-- AddCookerRecipe("cookpot",fish_sushi)
-- AddCookerRecipe("portablecookpot",fish_sushi)
-- AddCookerRecipe("xiuyuan_cookpot",fish_sushi)

-- local seajelly =
-- {
--     name = "seajelly",
-- 		test = function(cooker, names, tags) return tags.sea_jelly and tags.sea_jelly > 1 and names.saltrock and names.saltrock > 1 and not tags.meat end,
-- 		priority = 0,
-- 		weight = 1,
-- 		foodtype = "MEAT",
-- 		health = 20,
-- 		hunger = 40,
-- 		sanity = 3,
-- 		perishtime = TUNING.PERISH_SLOW,
-- 		cooktime = 2,
-- 		cookbook_atlas = "images/inventoryimages/creepindeep_cuisine.xml",		
-- 	floater = {"small", 0.05, 0.7},
--     tags = {},
-- }

-- AddCookerRecipe("cookpot",seajelly)
-- AddCookerRecipe("portablecookpot",seajelly)
-- AddCookerRecipe("xiuyuan_cookpot",seajelly)

RegisterInventoryItemAtlas("images/inventoryimages/sponge_piece.xml", "sponge_piece.tex" )
RegisterInventoryItemAtlas("images/inventoryimages/fish_fillet.xml", "fish_fillet.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/fish_fillet_cooked.xml", "fish_fillet_cooked.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/fish_n_chips.xml", "fish_n_chips.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/sponge_cake.xml", "sponge_cake.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/fish_gazpacho.xml", "fish_gazpacho.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/tuna_muffin.xml", "tuna_muffin.tex"  )
RegisterInventoryItemAtlas("ATLAS", "images/inventoryimages/sea_cucumber.xml", "sea_cucumber.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/tentacle_sushi.xml", "tentacle_sushi.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/fish_sushi.xml", "fish_sushi.tex"  )
--RegisterInventoryItemAtlas("images/inventoryimages/food/flower_sushi.xml", "flower_sushi.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/shrimp_tail.xml", "shrimp_tail.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/sea_petals.xml", "sea_petals.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/seagrass_chunk.xml", "seagrass_chunk.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/sea_petals.xml", "sea_petals.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/jelly_cap.xml", "jelly_cap.tex"  )
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seaweed.tex" )
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seaweed_cooked.tex"  )	
RegisterInventoryItemAtlas("images/inventoryimages/volcanoinventory.xml", "seaweed_dried"  )

RegisterInventoryItemAtlas("images/inventoryimages/creepindeep_cuisine.xml", "creepindeep_cuisine.tex"  )

--TheSim:RemapSoundEvent("dontstarve/music/music_dawn_stinger", "citd/music/music_dawn_stinger")
--TheSim:RemapSoundEvent("dontstarve/music/music_dusk_stinger", "citd/music/music_dusk_stinger")

AddPrefabPostInitAny(function(inst)
if inst.prefab == "trinket_12" then inst:AddTag("edible") end	
-------- Add bubbles on destruction
	if inst.components and (inst.components.workable or inst.components.health) then
		inst:AddComponent("deathbubbles")
	end	

	if inst:HasTag("player") then
	inst:AddComponent("bubbleblower")	------- Blow bubble underwater to players
	inst:AddComponent("oxygen")
---------- Custom Oxygen Levels
	if inst.prefab == "willow" then inst.components.oxygen.max = 80 end
	if inst.prefab == "wilson" then inst.components.oxygen.max = 100 end
	if inst.prefab == "wolfgang" then inst.components.oxygen.max = 150 end
	if inst.prefab == "waxwell" then inst.components.oxygen.max = 120 end
	if inst.prefab == "woodie" then inst.components.oxygen.max = 150 end
	if inst.prefab == "wendy" then inst.components.oxygen.max = 80 end
	if inst.prefab == "wickerbottom" then inst.components.oxygen.max = 100 end
	if inst.prefab == "wes" then inst.components.oxygen.max = 55 end
	if inst.prefab == "wigfrid" then inst.components.oxygen.max = 150 end
	if inst.prefab == "webber" then inst.components.oxygen.max = 85 end		
	if inst.prefab == "wurt" then inst.components.oxygen.max = 5000 inst.components.oxygen.current = 5000 end
		
	inst:ListenForEvent("runningoutofoxygen", function(inst, data)
		inst.components.talker:Say("Low Oxygen") --GetString(inst.prefab, "ANNOUNCE_OUT_OF_OXYGEN"))
    end)
	
	inst:ListenForEvent("startdrowning", 
		function(inst, data)
			if inst.HUD then
				inst.HUD.bloodover:UpdateState() 
			end
		end,
	inst)
	
	inst:ListenForEvent("stopdrowning", 
		function(inst, data) 
			if inst.HUD then
				inst.HUD.bloodover:UpdateState()
			end
		end,
	inst)
	

	-- WX78 is a robot!
	if inst.prefab == "wx78" then
		inst:AddTag("robot")
	end		
		
	end	

end)


AddPrefabPostInit("cave", function(inst)
if GLOBAL.TheWorld.ismastersim then  
inst:AddComponent("underwaterspawner")
end

end)

AddComponentPostInit("fueled", function(self)
------------------------
function self:Ignite(immediate, source, doer)
local map = GLOBAL.TheWorld.Map
local x, y, z = self.inst.Transform:GetWorldPosition()
local ground = map:GetTile(map:GetTileCoordsAtPoint(x, y, z))
if ground == GROUND.UNDERWATER_SANDY or ground == GROUND.UNDERWATER_ROCKY or (ground == GROUND.BEACH and GLOBAL.TheWorld:HasTag("cave")) or (ground == GROUND.BATTLEGROUND and GLOBAL.TheWorld:HasTag("cave")) or (ground == GROUND.PEBBLEBEACH and GLOBAL.TheWorld:HasTag("cave")) or (ground == GROUND.MAGMAFIELD and GLOBAL.TheWorld:HasTag("cave")) or (ground == GROUND.PAINTED and GLOBAL.TheWorld:HasTag("cave")) then return false end	
------------------------
    if not (self.burning or self.inst:HasTag("fireimmune")) then
        self:StopSmoldering()

        self.burning = true
        self.inst:ListenForEvent("death", OnKilled)
        self:SpawnFX(immediate)

        self.inst:PushEvent("onignite", {doer = doer})
        if self.onignite ~= nil then
            self.onignite(self.inst)
        end

        if self.inst.components.fueled ~= nil then
            self.inst.components.fueled:StartConsuming()
        end

        if self.inst.components.propagator ~= nil then
            self.inst.components.propagator:StartSpreading(source)
        end

        self:ExtendBurning()
    end
end	
end)
------------------------------------
modimport("scripts/widgets/oxygendisplay.lua")